#mvn-repo

##Introduction
My personal public maven repository  
[http://snowdream.github.io/mvn-repo](http://snowdream.github.io/mvn-repo)

##Getting Started
###Maven
Add to your pom.xml:
```xml
    <repositories>
        <repository>
            <id>snowdream-nexus-snapshots</id>
            <name>Snowdream Snapshots Repository</name>
            <url>https://github.com/snowdream/mvn-repo/raw/master/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>snowdream-nexus-releases</id>
            <name>Snowdream Releases Repository</name>
            <url>https://github.com/snowdream/mvn-repo/raw/master/releases</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>        
    </repositories>
```
or

```xml
    <repositories>
        <repository>
            <id>snowdream-nexus-snapshots</id>
            <name>Snowdream Snapshots Repository</name>
            <url>http://snowdream.github.io/mvn-repo/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>snowdream-nexus-releases</id>
            <name>Snowdream Releases Repository</name>
            <url>http://snowdream.github.io/mvn-repo/releases</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>        
    </repositories>
```


###Gradle
Add to your build.gradle:
```groovy
    repositories {
        mavenLocal()
        mavenCentral()
        maven { url "http://oss.sonatype.org/content/repositories/snapshots/" }
        maven { url "http://snowdream.github.io/mvn-repo/releases/" }
        maven { url "http://snowdream.github.io/mvn-repo/snapshots/" }
    }
```    
    
    
##Usage
For example:
###Maven
```xml
<dependency>
  <groupId>com.github.snowdream.android.util</groupId>
  <artifactId>log</artifactId>
  <version>1.0.2</version>
</dependency>
```

###Gradle
```groovy
    compile 'com.github.snowdream.android.util:log:1.0.2'
``` 


##Develop
Add to your pom.xml:
```xml
    <distributionManagement>
        <snapshotRepository>
            <id>snowdream-nexus-snapshots</id>
            <name>Snowdream Snapshots Repository</name>
            <url>https://github.com/snowdream/mvn-repo/raw/master/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>snowdream-nexus-releases</id>
            <name>Snowdream Releases Repository</name>
            <url>https://github.com/snowdream/mvn-repo/raw/master/releases</url>
        </repository>
    </distributionManagement>
```

```xml
    <distributionManagement>
        <snapshotRepository>
            <id>snowdream-nexus-snapshots</id>
            <name>Snowdream Snapshots Repository</name>
            <url>http://snowdream.github.io/mvn-repo/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>snowdream-nexus-releases</id>
            <name>Snowdream Releases Repository</name>
            <url>http://snowdream.github.io/mvn-repo/releases</url>
        </repository>
    </distributionManagement>
```
##Acknowledgements
> * Hosting Maven Repos on Github   
> [http://cemerick.com/2010/08/24/hosting-maven-repos-on-github/](http://cemerick.com/2010/08/24/hosting-maven-repos-on-github/)
> 
> * Maven Repositories on GitHub     
> [http://blog.kaltepoth.de/posts/2010/09/06/github-maven-repositories.html](http://blog.kaltepoth.de/posts/2010/09/06/github-maven-repositories.html)



##License
```
Copyright (C) 2013 Snowdream Mobile <yanghui1986527@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
